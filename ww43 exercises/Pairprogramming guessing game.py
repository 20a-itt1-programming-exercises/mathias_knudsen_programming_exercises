# Number guessing game
import random

def guessingGame():
    
    playerlives = 3
    playerscore = 0
    computerscore = 0
    playing = True
    tryAgn = ""
    
    while playing:
        if playerscore == 5:
            print("You won the best of five! GG")
            break
        elif computerscore == 5:
            print("I won the best of five! GG")
            break

        try:
     
            randNum = random.randint(0,9)

            print(f"you have {playerlives} attempts")
            userInp = input("Guess a number between 0 and 9\n")
            for char in userInp:
                if char.isdigit():
                    userInp = int(char)
                    break
            userInp = int(userInp)
            
            
            if userInp > 9 or userInp < 0: # Mercy for players who may accidentially have guessed outside the range
                print("That number is too high")
                continue

            elif playerlives == 1: # The player has run out of lives
                    
                    computerscore = computerscore + 1

                    while tryAgn != "y" or tryAgn != "n":
                        tryAgn = input("na na I won :-P Try again? (y/n)\n")
                    
                        if tryAgn.lower().startswith("y"):
                            tryAgn = "y"
                            playerlives = 3
                            break
                            
                        elif tryAgn.lower().startswith("n"):
                            tryAgn = "n"
                            print(f"\nHuman wins: {playerscore}\nComputer wins: {computerscore}\nThank you for playing my game!")
                            playing = False
                            break
                        else:
                            continue


            elif userInp != randNum: # The player guesses wrong
                    print("wrong guess, try again!")
                    playerlives = playerlives - 1

            elif userInp == randNum: # The player guesses correct
                    playerscore = playerscore + 1
                    
                    while tryAgn != "y" or tryAgn != "n":
                        tryAgn = input("You won! Try again? (y/n)\n")
                    
                        if tryAgn.lower().startswith("y"):
                            tryAgn = "y"
                            playerlives = 3
                            break
                            
                        elif tryAgn.lower().startswith("n"):
                            tryAgn = "n"
                            print(f"\nHuman wins: {playerscore}\nComputer wins: {computerscore}\nThank you for playing my game!")
                            playing = False
                            break
                        else:
                            continue


        except ValueError:
            print("Thats not even a number.... restarting")
            continue
guessingGame()