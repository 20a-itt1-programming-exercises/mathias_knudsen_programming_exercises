# Exercise 1
total = 0
count = 0
while True:
    try:
        number = input("Enter a number: ")
        if number == "done":
            avg = total/count
            print("The total sum of all numbers entered is " + str(total))
            print(f"The average of all numbers entered is {avg}")
            print(f"The count of all numbers entered is {count}")
            break
        else:
            total = float(total) + float(number)
            count = count + 1
    except:
        print("Bad data")
        continue