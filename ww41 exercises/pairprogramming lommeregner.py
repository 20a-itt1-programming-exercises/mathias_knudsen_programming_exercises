def add(num1,num2):
    sum = float(num1) + float(num2)
    return sum

def subtract(num1,num2):
    sum = float(num1) - float(num2)
    return sum

def divide(num1,num2):
    try:
        sum = float(num1) / float(num2)
    except ZeroDivisionError:
        print("Cannot divide by zero - please try again!")
        main()
    return sum

def multiply(num1,num2):
    sum = float(num1) * float(num2)
    return sum


def main():
    try:
        while True:
            userNum1Inp = input("Enter the first number or type 'done' to exit the program:" + "\n" + "> ")
            if str(userNum1Inp) == "done":
                print("Goodbye!")
                break
            elif userNum1Inp == "":
                continue
            userNum2Inp = input("Enter a second number or type 'done' to exit the program:" + "\n" + "> ")
            if str(userNum1Inp) == "done":
                print("Goodbye!")
                break
            elif userNum2Inp == "":
                continue
            try:
                userChoiceInp = input("Type '1' for addition" + "\n" + "Type '2' for subtraction" + "\n" + "Type '3' for division" + "\n" + "Type '4' for multiplication" + "\n" + "Type 'done' to exit the program!" + "\n" + "> ")
                if str(userChoiceInp) == "done":
                    break
                elif int(userChoiceInp) == 1:
                    print(f"{userNum1Inp} + {userNum2Inp} is: {add(userNum1Inp,userNum2Inp)}")
                elif int(userChoiceInp) == 2:
                    print(f"{userNum1Inp} - {userNum2Inp} is: {subtract(userNum1Inp,userNum2Inp)}")
                elif int(userChoiceInp) == 3:
                    print(f"{userNum1Inp} / {userNum2Inp} is: {divide(userNum1Inp,userNum2Inp)}")
                elif int(userChoiceInp) == 4:
                    print(f"{userNum1Inp} * {userNum2Inp} is: {multiply(userNum1Inp,userNum2Inp)}")
                else:
                    print("Number is not between 1 and 4")
            except ValueError:
                print("Only numbers and \"done\" is accepted as input!")
                continue
    except:
        print("Goodbye!")
main()