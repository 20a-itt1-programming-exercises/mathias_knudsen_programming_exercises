# Exercise 2
total = 0
count = 0
minVal = None
maxVal = None
while True:
    try:
        number = input("Enter a number: ")
        if number == "done":
            print(f"The total sum of all numbers entered is {total}")
            print(f"The count of all numbers entered is {count}")
            print(f"The minimin value entered is {minVal}")
            print(f"The maximum value entered is {maxVal}")
            break
        else:           
            total = float(total) + float(number)
            count = count + 1
            if minVal is None:
                minVal = float(number)
            elif float(number) < minVal:
                minVal = float(number)
            elif maxVal is None:
                maxVal = float(number)
            elif float(number) > maxVal:
                maxVal = float(number)
    except:
        print("Bad data")
        continue