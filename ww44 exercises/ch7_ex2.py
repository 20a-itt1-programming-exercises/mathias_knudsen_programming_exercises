# Exercise 2
count = 0
confidence = 0
while True:
    try:
        kagemand = input("Write a file name, like: mbox-short.txt for example\n")
        kage = open(kagemand)
        
        for x in kage:
            if x.startswith("X-DSPAM-Confidence:"):
                targetindex = x.find(" ")
                founddata = float(x[targetindex+1:])
                confidence += founddata
                count += 1
        print(confidence/count)
    except:
        print("That's not a valid filename")
        continue