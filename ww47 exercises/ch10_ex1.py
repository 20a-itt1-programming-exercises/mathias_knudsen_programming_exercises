# Exercise 1 Revise a previous program as follows: Read and parse the "From" lines and pull out the addresses from the line. Count the number of messages from each person using a dictionary.
# After all the data has been read, print the person with the most commits by creating a list of (count, email) tuples from the dictionary. Then sort the list in reverse order and print out
# the person who has the most commits.

fhand = open("mbox-short.txt")
userEmail = dict()
for line in fhand:
    line = line.rstrip()
    if not line.startswith("From "): continue
    words = line.split()
    email = words[1]
    #print(email)
    if email not in userEmail:
            userEmail[email] = 1
    else:
        userEmail[email] += 1
print(userEmail)

l = list()
for email,count in list(userEmail.items()):
    l.append((email,count))

l.sort()
for email,count in l:
    print(email,count)