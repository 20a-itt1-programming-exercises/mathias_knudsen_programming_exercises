#Exercise 2: Change your socket program so that it counts the number
#of characters it has received and stops displaying any text after it has
#shown 3000 characters. The program should retrieve the entire docu-
#ment and count the total number of characters and display the count
#of the number of characters at the end of the document.

import socket

count = 0
url = input("Type URL: ")
chopURL = url.split("/")
host = chopURL[2]
print(host)
mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect((host, 80))
cmd = f'GET {url} HTTP/1.0\r\n\r\n'.encode()
mysock.send(cmd)

while True:
    data = mysock.recv(512)
    if len(data) < 1:
        break
    for character in data:
        count += 1
    if count < 3000:
        print(data.decode(),end='')
    else:
        continue
print(f"\n\nNumber of characters: {count}")

mysock.close()
