#Exercise 7
def computegrade(score):
    try:
        score = float((input("Enter score between 0 and 1\n")))
        if score >= 0.9:
            return("A")
        elif score >= 0.8:
            return("B")
        elif score >= 0.7:
            return("C")
        elif score >= 0.6:
            return("D")
        elif score < 0.6:
            return("F")
        else:
            return("Bad score")
    except:
        return("Bad score")
print(computegrade(0))