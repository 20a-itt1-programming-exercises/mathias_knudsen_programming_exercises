# Exercise 1: Write a function called chop that takes a list and modifies
# it, removing the first and last elements, and returns None. Then write
# a function called middle that takes a list and returns a new list that
# contains all but the first and last elements.
def chop(t):
    del t[0]
    del t[-1]
    return None
daList = [1, 2, 3, 4, 5]
daList2 = [1, 2, 3, 4, 5]
chop(daList)
print(daList)

def middle(t):
    chop(t)
    nt = t
    return nt
print(middle(daList2))