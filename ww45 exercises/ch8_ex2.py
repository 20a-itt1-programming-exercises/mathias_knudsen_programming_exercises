# Exercise 2
# Having a line starting with from but less than 2 words would cause it to fail. So I made sure that the line starting with from had more than one word.
fhand = open('customtext.txt')
count = 0
for line in fhand:
    words = line.split()
    # print('Debug:', words)
    if len(words) == 0 : continue
    if words[0] != 'From' : continue
    if words[0] == "From" and len(words) > 2:
        print(words[2])

