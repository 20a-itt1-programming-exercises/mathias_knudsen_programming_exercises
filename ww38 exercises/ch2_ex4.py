# Exercise 4
# Write the value of the expression and the type.
#width = 17
#height = 12.0
# 1. width//2 = 1
# 2. width/2.0 = 8.5
# 3. height/3 = 4
# 4. 1 + 2 * 5 = 11
import math
width = 17
height = 12.0
answer1 = width//2
answer2 = width/2.0
answer3 = height/3
answer4 = 1+2*5
print("width//2 =",answer1)
print("Answer 1 is a",type(answer1))
print("width/2.0 =",answer2)
print("Answer 2 is a",type(answer2))
print("width/2.0 =",answer3)
print("Answer 3 is a",type(answer3))
print("height/3 =",answer4)
print("Answer 4 is a",type(answer4))
input()