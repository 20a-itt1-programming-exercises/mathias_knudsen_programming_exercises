'''
Exercise 3
Write a program to read through a mail log, build a his-
togram using a dictionary to count how many messages have come from
each email address, and print the dictionary.
'''

'''
Exercise 4: Add code to the above program to figure out who has the
most messages in the file. After all the data has been read and the dic-
tionary has been created, look through the dictionary using a maximum
loop (see Chapter 5: Maximum and minimum loops) to find who has
the most messages and print how many messages the person has.
fhand = open("mbox-short.txt")
'''
userEmail = dict()
for line in fhand:
    line = line.rstrip()
    if not line.startswith("From "): continue
    words = line.split()
    email = words[1]
    print(email)
    if email not in userEmail:
            userEmail[email] = 1
    else:
        userEmail[email] += 1
print(userEmail)
maxmessage = userEmail.get(email,)
print(email, maxmessage)