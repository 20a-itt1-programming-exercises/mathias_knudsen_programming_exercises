'''
Exercise 2
Write a program that categorizes each mail message by
which day of the week the commit was done. To do this look for lines
that start with “From”, then look for the third word and keep a running
count of each of the days of the week. At the end of the program print
out the contents of your dictionary (order does not matter).
'''
try:
    filename = input("Input filename here\n")
    fhand = open(filename)
except:
    print("Filename not found")
    exit()

days = dict()
for line in fhand:
    line = line.strip()
    if not line.startswith("From "): continue
    words = line.split()
    print(words)
    day = words[2]
    if day not in days:
        days[day] = 1
    else:
        days[day] += 1
print(days)