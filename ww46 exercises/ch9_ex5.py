'''
Exercise 5: This program records the domain name (instead of the
address) where the message was sent from instead of who the mail came
from (i.e., the whole email address). At the end of the program, print
out the contents of your dictionary.
'''

fhand = open("mbox-short.txt")
userEmail = dict()
for line in fhand:
    line = line.rstrip()
    if not line.startswith("From "): continue
    words = line.split()
    email = words[1]
    atpos = email.find("@")
    emailDomain = email[atpos+1:]
    print(emailDomain)
    if emailDomain not in userEmail:
            userEmail[emailDomain] = 1
    else:
        userEmail[emailDomain] += 1
print(userEmail)

