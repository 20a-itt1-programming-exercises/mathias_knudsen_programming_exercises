#Exercise 2: Write a program to look for lines of the form:
#New Revision: 39772
#Extract the number from each of the lines using a regular expression
#and the findall() method. Compute the average of the numbers and
#print out the average as an integer.
from statistics import mean
import re
xlist = []
hand = open('mbox.txt')
for line in hand:
    x = re.findall('^New Revision.* ([0-9]+)', line)
    if len(x) > 0:
        xlist.append(int(x[0]))
print(round(mean(xlist),0))