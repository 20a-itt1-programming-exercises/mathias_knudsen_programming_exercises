#Exercise 1: Write a simple program to simulate the operation of the
#grep command on Unix. Ask the user to enter a regular expression and
#count the number of lines that matched the regular expression:

import re
count = 0
sumList = []
inpReg = input("Enter a regular expression: ")
hand = open('mbox.txt')
for line in hand:
    line = line.rstrip()
    if re.findall(inpReg, line):
        count += 1
    else:
        continue
print(f'mbox.txt had {count} that matched {inpReg}')
