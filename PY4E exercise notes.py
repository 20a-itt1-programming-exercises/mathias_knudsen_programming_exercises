Exercise 1:
    C) Long term storage and store data even beyond a powercycle.

Exercise 2:
    Simply making the computer do something is a program.
    As simple as printing "Hello world" is defined as a program.

Exercise 3:
    A compiler is very simply a program that converts highlevel code into low level.
    An intepreter is a program that instantly compiles code and executes it.

Exercise 4:
    a) The interpreter interprets your code directly to machine code.

Exercise 5:
    Mistyped "print" as "primt".

Exercise 6:
    B) main memory also called the ram.

Exercise 7:
    b) 44

Exercise 8:
    1) The part of the brain that does the thinking.
    2) The part of the brain that rembers short term.
    3) The part of the brain that remembers long term.
    4) Your ears, sense of touch, nose, taste etc.
    5) Mouth, or voice. Everything you can use to interact with the world around you.

Exercise 9:
    Check the problem code that python gives you and try to locate where the syntax error occured.
    It might just be the a mistype like "primt" instead of "print".
